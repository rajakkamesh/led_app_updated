package com.software.damyant.LetsLED.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.software.damyant.LetsLED.AppConstant;
import com.software.damyant.LetsLED.R;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class to show lading screen
 */
public class LandingScreen extends Activity{
    private Button submit;
    private Context context;
    private VideoView videoView;
    private RelativeLayout rl_video;
    private WebView web_view;
    private LinearLayout linearLayout;
    private ImageView imageView_play,imageview_pause;

    //TODO  Called when the activity is first created.
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_screen);
        submit=(Button)findViewById(R.id.button_start);
        context = this;
        videoView = (VideoView) findViewById(R.id.videoView);
        imageView_play = (ImageView) findViewById(R.id.imageView_play);
        web_view = (WebView) findViewById(R.id.webview);
        rl_video = (RelativeLayout) findViewById(R.id.rl_videoview);
//        MediaController mediaController = new MediaController(this);
//        mediaController.setAnchorView(videoView);
        imageview_pause = (ImageView)findViewById(R.id.imageView_pause);
        linearLayout= (LinearLayout)findViewById(R.id.ll_about);
        imageView_play.setVisibility(View.GONE);
        rl_video.setVisibility(View.VISIBLE);
        imageview_pause.setVisibility(View.VISIBLE);
        web_view.setBackgroundColor(Color.TRANSPARENT);
        WebSettings webSettings = web_view.getSettings();
        webSettings.setDefaultFontSize(14);
        web_view.loadData(AppConstant.landing_screen_text, "text/html", "utf-8");

        setClickEvent();

        try{
            //TODO  local path of video file
            String local_file_path = "android.resource://" + context.getPackageName() + "/" + R.raw.introvideo;
            videoView.setVideoURI(Uri.parse(local_file_path));
//            videoView.setMediaController(mediaController);
            videoView.seekTo(100);
        }catch(Exception e){
            Log.e("Error", e.getMessage());
        }

        videoView.requestFocus();
        videoView.start();//TODO Sart Video


    }


    private void setClickEvent() {
        //TODO called when click on play button
        imageView_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView_play.setVisibility(View.GONE);
                rl_video.setVisibility(View.VISIBLE);
                imageview_pause.setVisibility(View.VISIBLE);
                videoView.start();

            }
        });
        //TODO called when video completes
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                imageView_play.setVisibility(View.VISIBLE);
                imageview_pause.setVisibility(View.GONE);
            }
        });

        //TODO called when click on pause button
        imageview_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView_play.setVisibility(View.VISIBLE);
                rl_video.setVisibility(View.VISIBLE);
                imageview_pause.setVisibility(View.GONE);
                videoView.pause();

            }
        });
//        linearLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AppConstant.showDialogs(context);
//            }
//        });

        //TODO called when click on Start now button
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LandingScreen.this, Room_Chooser.class);
                startActivity(i);
                finish();
            }
        });
    }
    //TODO called when navigate to another activity
    @Override
    protected void onPause() {
        super.onPause();
        imageView_play.setVisibility(View.VISIBLE);
        rl_video.setVisibility(View.VISIBLE);
        imageview_pause.setVisibility(View.GONE);
        videoView.pause();
    }
    //TODO called when activity resume
    @Override
    protected void onResume() {
        super.onResume();
        rl_video.setVisibility(View.VISIBLE);
        imageView_play.setVisibility(View.GONE);
        imageview_pause.setVisibility(View.VISIBLE);
        videoView.seekTo(videoView.getCurrentPosition());
        videoView.start();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}