package com.software.damyant.LetsLED.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.software.damyant.LetsLED.AppConstant;
import com.software.damyant.LetsLED.R;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class use to show room chooser activity
 */
public class Room_Chooser extends Activity implements View.OnClickListener {
    public static int num = 0;
    private Context context;
    private String roomname[] = {"Home", "Shop", "Office", "Industry"};
    private String roomdiscription[] = {"", "Office work is visually demanding and requires good lighting for safety, health comfort and productivity.<br><br> People must see printed, handwritten or displayed documents clearly and in the right color both low and high light levels (a cause of glare) cause problems.<br><br> Use diffused Panel lights, down-lights, and inverted tube-lights for workspace and choose CCT as per your decor.", "As lighting constitutes a large percentage of energy costs in a store, good retail lighting will sell more products at less cost.<br><br> Use down-lights and 4ft tubes hung from ceiling to get the maximum lux on the products.", ""};

    //TODO  Called when the activity is first created.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.room_chooser2);
        findViewById(R.id.ll1).setOnClickListener(this);
        findViewById(R.id.ll2).setOnClickListener(this);
        findViewById(R.id.ll3).setOnClickListener(this);
        findViewById(R.id.ll4).setOnClickListener(this);
        findViewById(R.id.ll_ques_home).setOnClickListener(this);
        findViewById(R.id.ll_ques_office).setOnClickListener(this);
        findViewById(R.id.ll_ques_shop).setOnClickListener(this);
        findViewById(R.id.ll_ques_industry).setOnClickListener(this);
        findViewById(R.id.ll_about).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        //TODO  on click of room room value is set in num for further use
        if (view.getId() == R.id.ll_ques_home) {
            num = 10;
            Intent i = new Intent(Room_Chooser.this, Home_Room_Chooser.class);
            startActivity(i);
            return;
        } else if (view.getId() == R.id.ll_ques_office) {
            showDialogs(1);
            return;
        } else if (view.getId() == R.id.ll_ques_shop) {
            showDialogs(2);
            return;
        } else if (view.getId() == R.id.ll_ques_industry) {
//            showDialogs(3);
            return;
        } else if (view.getId() == R.id.ll1) {
            num = 10;
            Intent i = new Intent(Room_Chooser.this, Home_Room_Chooser.class);
            startActivity(i);
        } else if (view.getId() == R.id.ll2) {
            num = 5;
        } else if (view.getId() == R.id.ll4) {
            num = 6;
        } else if (view.getId() == R.id.ll3) {
            num = 7;
        }else if (view.getId() == R.id.ll_about) {
            AppConstant.showDialogs(context,1);
        }

        //TODO  on room click even activity navigate to value insert activity
        if (view.getId() != R.id.ll1 && view.getId() != R.id.ll_about) {
            Intent i = new Intent(Room_Chooser.this, ValueInsert.class);
            startActivity(i);
        }
    }

    //TODO if user click on details button on room then show info about room
    public void showDialogs(int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.home_layout);
        dialog.setTitle("" + roomname[position]);
        TextView title = (TextView) dialog.findViewById(R.id.textview_alert_title);
        title.setText("" + roomname[position]);
        //TODO set the custom dialog components - text, image and button
        WebView alert_web = (WebView) dialog.findViewById(R.id.textview_alert_detail);
        String youtContentStr = String.valueOf(Html
                .fromHtml("<![CDATA[<body style=\"text-align:justify;background-color:#00222222;\">"
                        + roomdiscription[position] +
                        "</body>]]>"));
        alert_web.setBackgroundColor(Color.TRANSPARENT);
        WebSettings webSettings = alert_web.getSettings();
        webSettings.setDefaultFontSize(15);
        alert_web.loadData(youtContentStr, "text/html", "utf-8");
        dialog.setCanceledOnTouchOutside(false);
        ImageView dialogButton = (ImageView) dialog.findViewById(R.id.button_alert_ok);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //TODO if user click on back button then dialog appear
    public void showExitDialog() {

        final Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.exit_dailog);
        Button button_ok = (Button) builder.findViewById(R.id.button_ok);
        Button button_cancel = (Button) builder.findViewById(R.id.button_cancel);
        //TODO if user click on ok button
        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.cancel();
                finish();
            }
        });
        //TODO if user click on cancel button
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.cancel();
            }
        });
        builder.show();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    //TODO if user press back button on home screen then open exit dialog box
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showExitDialog();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
