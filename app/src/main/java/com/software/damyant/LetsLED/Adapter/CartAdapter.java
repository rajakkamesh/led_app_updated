package com.software.damyant.LetsLED.Adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.software.damyant.LetsLED.Activity.CartList;
import com.software.damyant.LetsLED.Activity.MainActivity;
import com.software.damyant.LetsLED.AppConstant;
import com.software.damyant.LetsLED.CartListDataBase;
import com.software.damyant.LetsLED.R;
import com.software.damyant.LetsLED.lasy_image_loader.ImageLoader;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class use for making list view in cartList activity
 */
public class CartAdapter extends BaseAdapter {
    private Context context;
    private String[] cart_data, cart_pname, cart_pprice, cart_pdiscription;
    private static LayoutInflater inflater = null;
    private ImageLoader imageLoader;
    private int quantity[];
    private CartListDataBase cartdata;

    //TODO constructor of CartAdaptor
    public CartAdapter(Context context, String[] d, String[] n, String[] dis, String[] p, int[] quanity) {
        this.context = context;
        cartdata = MainActivity.cartdata;
        cart_data = d;
        cart_pname = n;
        cart_pdiscription = dis;
        cart_pprice = p;
        quantity = quanity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(context.getApplicationContext());
    }

    //TODO return length of data
    @Override
    public int getCount() {
        return cart_data.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    //TODO call each time and return a view of list
    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {

        View vi = view;
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            vi = inflater.inflate(R.layout.listview_row_cart, null);
            holder.edit_quantity = (EditText) vi.findViewById(R.id.edittext_cart_quantity);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final TextView cart_name = (TextView) vi.findViewById(R.id.textview_name_cartlist_product);
        final TextView cart_price = (TextView) vi.findViewById(R.id.textview_price_in_cart);
        final TextView cart_total = (TextView) vi.findViewById(R.id.textview_total_price_item);
        ImageView cart_image = (ImageView) vi.findViewById(R.id.imageview_selectpro_incart_image);
        ImageView close_image = (ImageView) vi.findViewById(R.id.close_img);

        ImageButton increase = (ImageButton) vi.findViewById(R.id.imagebutton_cart_increase);
        ImageButton decrease = (ImageButton) vi.findViewById(R.id.imagebutton_cart_decrease);

        //TODO called when List product remove from cart
        close_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartdata.updateSelected(Integer.parseInt(cart_pdiscription[i]), 0);
                cartdata.deleteProduct(Integer.parseInt(cart_pdiscription[i]));
                ((CartList) context).updateGrandTotal();
                Intent intent = ((CartList) context).getIntent();
                ((CartList) context).finish();
                context.startActivity(intent);

            }
        });

        // TODO called when click on decrease quantity button
        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor cursor = cartdata.getQunity(Integer.parseInt(cart_pdiscription[i]));
                if (cursor.moveToFirst()) {
                    quantity[i] = Integer.parseInt(cursor.getString(0));
                }
                if (quantity[i] <= 1) {
                    cartdata.updateQuanity(Integer.parseInt(cart_pdiscription[i]), 1);
                    cart_total.setText(" " + quantity[i] * Float.parseFloat(cart_pprice[i]));
                    holder.edit_quantity.setText("" + 1);
                    ((CartList) context).updateGrandTotal();
                } else {

                    quantity[i] = quantity[i] - 1;
                    cartdata.updateQuanity(Integer.parseInt(cart_pdiscription[i]), quantity[i]);

                    cart_total.setText(" " + quantity[i] * Float.parseFloat(cart_pprice[i]));
                    holder.edit_quantity.setText("" + (quantity[i]));
                    ((CartList) context).updateGrandTotal();
                }


            }
        });
        // TODO called when click on increase quantity button
        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor cursor = cartdata.getQunity(Integer.parseInt(cart_pdiscription[i]));
                if (cursor.moveToFirst()) {
                    quantity[i] = Integer.parseInt(cursor.getString(0));
                }
                if (quantity[i] < AppConstant.CART_PRODUCT_LIMIT) {
                    quantity[i] = quantity[i] + 1;
                    cartdata.updateQuanity(Integer.parseInt(cart_pdiscription[i]), quantity[i]);
                    cart_total.setText(" " + quantity[i] * Float.parseFloat(cart_pprice[i]));
                    holder.edit_quantity.setText("" + quantity[i]);
                    ((CartList) context).updateGrandTotal();
                }
            }
        });
        //we need to update adapter once we finish with editing
        holder.edit_quantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if((holder.edit_quantity.getText().toString()).equals("")||Integer.parseInt(holder.edit_quantity.getText().toString())==0){
                        quantity[i] = 1;
                        cartdata.updateQuanity(Integer.parseInt(cart_pdiscription[i]), quantity[i]);
                    }else{
                    if (Integer.parseInt(holder.edit_quantity.getText().toString()) < AppConstant.CART_PRODUCT_LIMIT) {
                        quantity[i] = Integer.parseInt(holder.edit_quantity.getText().toString());
                        cartdata.updateQuanity(Integer.parseInt(cart_pdiscription[i]), quantity[i]);
                    }else{
                        holder.edit_quantity.setText("" + quantity[i]);
                        Toast.makeText(context,"please enter quantity less then 1000",Toast.LENGTH_LONG).show();
                    }
                }
                    cart_total.setText(" " + quantity[i] * Float.parseFloat(cart_pprice[i]));
                    ((CartList) context).updateGrandTotal();
                }
            }
        });

        // TODO to set values in list
        holder.edit_quantity.setText("" + quantity[i]);
        cart_name.setText(cart_pname[i]);
        cart_price.setText(cart_pprice[i]);
        imageLoader.DisplayImage(cart_data[i], cart_image);
        cart_total.setText(" " + quantity[i] * Float.parseFloat(cart_pprice[i]));
        return vi;
    }

}
class ViewHolder {
    EditText edit_quantity;
}