package com.software.damyant.LetsLED.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.software.damyant.LetsLED.AppConstant;
import com.software.damyant.LetsLED.R;
import com.software.damyant.LetsLED.R.*;
import com.software.damyant.LetsLED.Camera.ShowCamera;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class use to detect color of light, furniture and wall using camera
 */
public class ColorDetectorScreen extends Activity {

    private ShowCamera showCamera;
    private ImageView imageView4;
    private FrameLayout preview;
    private DrawOnTop mDraw;
    private View  wall_color, furniture_color;
    private Button submit;
    private TextView textview_error_cam;
    private RelativeLayout af_casing;
    private LinearLayout ll_home;
    public static int click_num = 0;
    private MediaPlayer mediaPlayer;
    private LinearLayout about_linearLayout;
    // TODO pointer is true if device has camera otherwise false
    boolean pointer = true;


    //TODO Called when the activity is first created

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.color_detector_screen);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        try {

            Display dis = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            dis.getSize(size);
            int width = size.x;
            int height = size.y;
            submit = (Button) findViewById(id.button_calculate);
            af_casing = (RelativeLayout) findViewById(id.af_casing);
            textview_error_cam = (TextView) findViewById(id.textview_error_camera);
            mediaPlayer = MediaPlayer.create(this, raw.camera1);
            ll_home = (LinearLayout) findViewById(id.ll_home);
            about_linearLayout = (LinearLayout) findViewById(id.ll_about);
            imageView4 = (ImageView) findViewById(id.imageView4);
            wall_color = findViewById(id.myRectangleView2);
            furniture_color = findViewById(id.myRectangleView3);
            preview = (FrameLayout) findViewById(id.camera_preview);
            showCamera = new ShowCamera(this,wall_color, furniture_color);
            mDraw = new DrawOnTop(this, preview,width,height);
            preview.addView(showCamera);
            final Context context = this;
            PackageManager packageManager = context.getPackageManager();

            // TODO check for back camera is available or not
            if (!packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                preview.setVisibility(View.GONE);
                af_casing.setVisibility(View.VISIBLE);
                mediaPlayer.stop();
                pointer = false;
                textview_error_cam.setText(getResources().getString(R.string.no_camera_msg));
                textview_error_cam.setTextColor(getResources().getColor(R.color.camera_msg_color));
                submit.setText(string.next);
            }

            // TODO when click on home button it will navigate to Room chose or home screen
            about_linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppConstant.showDialogs(context,0);

                }
            });
        } catch (Exception e) {
            preview.setVisibility(View.GONE);
            af_casing.setVisibility(View.VISIBLE);
            mediaPlayer.stop();
            pointer = false;
            textview_error_cam.setText(getResources().getString(R.string.err_camera_msg));
            textview_error_cam.setTextColor(getResources().getColor(R.color.camera_msg_color));
            submit.setText(string.next);
        }

        // TODO if device has camera then set frame on preview
        if (pointer == true) {
            addContentView(mDraw, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        }

        submit.setVisibility(View.VISIBLE);

        //TODO On click of home button it will navigate to Room chooser screen
        ll_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(ColorDetectorScreen.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

        // TODO when click on home button it will navigate to Room chose or home screen
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(ColorDetectorScreen.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

            }
        });


        // TODO when click on button
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pointer == false) {
                    click_num = 0;
                    Intent i = new Intent(ColorDetectorScreen.this, ResultScreen2.class);
                    startActivity(i);
                } else {
                    if (click_num == 0) {
                        mediaPlayer.start();
                        submit.setText(string.capture_furniture);
                    } else if (click_num == 1) {
                        mediaPlayer.start();
                        submit.setText(string.next);
                    } else if (click_num == 2) {
                        Intent i = new Intent(ColorDetectorScreen.this, ResultScreen2.class);
                        startActivity(i);
                    }
                }
                click_num++;
            }
        });

    }

    // TODO restart preview after awake from phone sleeping
    @Override
    public void onResume() {
        // TODO reset button value on resume

        click_num = 0;
        if (pointer == false) {
            submit.setText(string.next);
        } else {
            submit.setText(string.capture_wall);
            showCamera.myStartPreview();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (showCamera != null) {
            showCamera.myStopPreview();  // TODO stop preview in case phone is going to sleep
        }
        super.onPause();
    }

}

//TODO class use to draw frame on camera preview
class DrawOnTop extends View {
    FrameLayout frameLayout;
    int width,height;

    public DrawOnTop(Context context, FrameLayout frameLayout,int width,int height) {
        super(context);
        this.frameLayout = frameLayout;
        this.width=width;
        this.height=height;
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GREEN);
        paint.setStrokeWidth(2);
        canvas.drawRect(frameLayout.getX()+frameLayout.getWidth() / 2 - width*30/100, frameLayout.getY()+frameLayout.getHeight() / 2- height*10/100, frameLayout.getX()+frameLayout.getWidth() / 2 + width*30/100, frameLayout.getY()+frameLayout.getHeight() / 2 + height*10/100, paint);
        paint.setColor(getResources().getColor(R.color.yellow_color));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        canvas.drawCircle(frameLayout.getX()+frameLayout.getWidth() / 2, frameLayout.getY()+frameLayout.getHeight() / 2, 20, paint);
        super.onDraw(canvas);
    }

}
