package com.software.damyant.LetsLED.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.software.damyant.LetsLED.AppConstant;
import com.software.damyant.LetsLED.R;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class use to show room chooser activity
 */
public class Home_Room_Chooser extends Activity implements View.OnClickListener {
    public static int num = 0;
    Context context;
    private String roomname[]={"Living room","Bedroom","Kitchen","Dining Room","Bathroom","Study Room"};
    private String roomdiscription[]={"Lighting fixtures and colour of light should complement your existing decor.<br><br> Check if your decor is ornate, rustic, modern, minimalist or a combination of aesthetics.<br><br>Use white bulbs for clean, white light in a contemporary setting or, for a warm, homely feel, use warm light bulbs.<br><br>Direct Tube lights are best avoided in living room, but may be used in Covings.<br><br> Putting your lighting fixtures on dimmable/Colour change bulbs allows for added control over the ambiance in your family room.","Be sure to keep ambient lighting, accent lighting (to highlight art and other features), and task lighting (to provide focused light for reading and other activities).<br><br> Use warm and White colours as per your decor. Use dimmable bulbs to suit your mood.","The kitchen is often the busiest part of the house. Not only are your meals prepared here, but your family gather here as well.<br><br> Adequate lighting is a must for performing all your culinary needs, helping your kids with their homework, and reading the paper.<br><br>\n" +
            "Sketch a plan of your kitchen that focuses on activity areas and then decide what kind of light each area will need: general, task, accent, or decorative. Use higher watt bulbs in task areas.<br><br>\n" +
            "Go with LED tubes or downlights centered over a workspace. LED tubelights under cabinet fixtures are also a cost efficient lighting source. In open areas over sinks use recessed down-lights mounted directly over the sink.<br><br>\n","Your dining table is in place, chairs are pushed in, and the table is set.<br><br> Create a focal point with a glass or plastic pendant lighting hanging 24 to 30 inches above the table.<br><br> When table space is limited, wall lamps are a good alternative. Choose LED ultra lumen bulbs for your fittings.","Since you begin and end your day in the bathroom, consider which fixtures and lights would work best for mirrors, sink and shower Do not depend on one ceiling fixture to light up.<br><br>Use 2ft LED deco tubes over mirrors and downlights for ceilings.<br><br>Use LED bulbs in fixtures.","Appropriate Lighting of study room is of paramount importance for reading and writing.<br><br> Lighting source must be adequate to avoid strain on eyes to avoid deterioration of eyesight and other harmful effects on health & mental peace. Standards for minimum Lux must be followed."};


    //TODO  Called when the activity is first created.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(R.layout.room_chooser_new);
        findViewById(R.id.ll1).setOnClickListener(this);
        findViewById(R.id.ll2).setOnClickListener(this);
        findViewById(R.id.ll3).setOnClickListener(this);
        findViewById(R.id.ll4).setOnClickListener(this);
        findViewById(R.id.ll5).setOnClickListener(this);
        findViewById(R.id.ll6).setOnClickListener(this);
        findViewById(R.id.ll_ques_living).setOnClickListener(this);
        findViewById(R.id.ll_ques_dining).setOnClickListener(this);
        findViewById(R.id.ll_ques_kitchen).setOnClickListener(this);
        findViewById(R.id.ll_ques_bed).setOnClickListener(this);
        findViewById(R.id.ll_ques_bath).setOnClickListener(this);
        findViewById(R.id.ll_ques_study).setOnClickListener(this);
        findViewById(R.id.imageView4).setOnClickListener(this);
        findViewById(R.id.ll_about).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        //TODO  on click of room room value is set in num for further use
        if (view.getId() == R.id.ll_ques_living) {
            showDialogs(0);
            return;
        }else if (view.getId() == R.id.ll_ques_bed) {
            showDialogs(1);
            return;
        }else if (view.getId() == R.id.ll_ques_kitchen) {
            showDialogs(2);
            return;
        }else if (view.getId() == R.id.ll_ques_dining) {
            showDialogs(3);
            return;
        }else if (view.getId() == R.id.ll_ques_bath) {
            showDialogs(4);
            return;
        }else if (view.getId() == R.id.ll_ques_study) {
            showDialogs(5);
            return;
        }
        else if (view.getId() == R.id.ll_about) {
            AppConstant.showDialogs(context,1);
            return;
        }
        else if (view.getId() == R.id.ll1) {
            num = 1;
        }
        else if (view.getId() == R.id.ll2) {
            num = 2;
        } else if (view.getId() == R.id.ll3) {
            num = 3;
        } else if (view.getId() == R.id.ll4) {
            num = 4;
        }
        else if (view.getId() == R.id.ll5) {
            num =8;
        } else if (view.getId() == R.id.ll6) {
            num = 9;
        }

         //TODO  on room click even activity navigate to value insert activity
        if (view.getId() != R.id.imageView4){
            Intent i = new Intent(Home_Room_Chooser.this, ValueInsert.class);
            startActivity(i);
        }else{
            Intent i = new Intent(Home_Room_Chooser.this, Room_Chooser.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }


    }

    //TODO method use to show information about room
    public void showDialogs(int position){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.home_layout);
        dialog.setTitle("" + roomname[position]);
        TextView title = (TextView) dialog.findViewById(R.id.textview_alert_title);
        title.setText("" +roomname[position]);
        //TODO set the custom dialog components - text, image and button
        WebView alert_web = (WebView) dialog.findViewById(R.id.textview_alert_detail);
        String youtContentStr = String.valueOf(Html
                .fromHtml("<![CDATA[<body style=\"text-align:justify;background-color:#00222222;\">"
                        + roomdiscription[position] +
                        "</body>]]>"));
        alert_web.setBackgroundColor(Color.TRANSPARENT);
        WebSettings webSettings = alert_web.getSettings();
        webSettings.setDefaultFontSize(15);
        alert_web.loadData(youtContentStr, "text/html", "utf-8");
        dialog.setCanceledOnTouchOutside(false);
        ImageView dialogButton = (ImageView) dialog.findViewById(R.id.button_alert_ok);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }


}
