package com.software.damyant.LetsLED.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.software.damyant.LetsLED.AppConstant;
import com.software.damyant.LetsLED.CartListDataBase;
import com.software.damyant.LetsLED.R;
import com.software.damyant.LetsLED.R.*;
import com.software.damyant.LetsLED.Service.SpeedometerView;


/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class used to detect light Lux
 */
public class DetectLuxActivity extends Activity implements SensorEventListener {

    private boolean isSpeedometerStarted = false;
    private TextView  textView_recommed_lux;
    private TextView textView_digital_speedometer, error,textview_lux_diff,textview_room;
    private SpeedometerView speedometer;
    private SensorManager mSensorManager;
    private MediaPlayer mediaPlayer;
    private LinearLayout ll_home,ll_diff;
    private CartListDataBase cartdata;
    private Button lux_dect_next;
    private ImageView  imageView4;
    private Context context;
    public static String room_name;
    private Sensor mLight;
    private float MAX_LUX;
    private float lux;
    private float temp;


    //TODO  Called when the activity is first created.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_detect_lux);
        cartdata = MainActivity.cartdata;
        speedometer = (SpeedometerView) findViewById(id.speedometer);
//        textView_start = (TextView) findViewById(id.textView_start);
        textview_lux_diff= (TextView) findViewById(id.textview_lux_diff);
        textView_digital_speedometer = (TextView) findViewById(id.textView_digital_speedometer);
        error = (TextView) findViewById(id.textview_error);
        textview_room= (TextView) findViewById(id.textview_room);
        mediaPlayer = MediaPlayer.create(context, raw.beep);
        lux_dect_next = (Button) findViewById(id.button_next);
        lux_dect_next.setVisibility(View.GONE);
        imageView4 = (ImageView) findViewById(id.imageView4);
        ll_home=(LinearLayout)findViewById(id.ll_home);
        ll_diff=(LinearLayout)findViewById(id.ll_diff);
        for (int i = 1; i <= AppConstant.TOTAL_NUMBER_ROOM; i++) {
            if (Room_Chooser.num == 10) {
                if (Home_Room_Chooser.num == i) {
                    Cursor c = cartdata.getRoomLux(Home_Room_Chooser.num);
                    if (c.moveToFirst()) {
                        MAX_LUX = Float.parseFloat(c.getString(2));
                    }
                    Cursor c1 = cartdata.getRoomLux(Home_Room_Chooser.num);
                    if (c1.moveToFirst()) {
                        room_name = c1.getString(1);
                    }
                }
            } else {
                if (Room_Chooser.num == i) {
                    Cursor c = cartdata.getRoomLux(Room_Chooser.num);
                    if (c.moveToFirst()) {
                        MAX_LUX = Float.parseFloat(c.getString(2));
                    }
                    Cursor c1 = cartdata.getRoomLux(Room_Chooser.num);
                    if (c1.moveToFirst()) {
                        room_name = c1.getString(1);
                    }
                }
            }
        }
        //TODO On click of home button it will navigate to Room chooser screen
        ll_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(DetectLuxActivity.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

        //TODO On click of home button it will navigate to Room chooser screen
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(DetectLuxActivity.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

            }
        });

        textView_recommed_lux = (TextView) findViewById(id.textView_recommed_lux);
        textView_recommed_lux.setText(room_name+" : Recommended " + MAX_LUX + " lux");

        speedometer.setLabelConverter(new SpeedometerView.LabelConverter() {
            @Override
            public String getLabelFor(double progress, double maxProgress) {
                return String.valueOf((int) Math.round(progress));
            }
        });

        //TODO On click of next button it will navigate to color detection screen
        lux_dect_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DetectLuxActivity.this, ColorDetectorScreen.class);
                startActivity(i);
            }
        });

        //TODO configure value range and ticks
        speedometer.setMinSpeed(20);
        speedometer.setMaxSpeed(500);
        speedometer.setMajorTickStep(50);
        speedometer.setMinorTicks(0);

        //TODO Configure value range colors
        speedometer.addColoredRange(0, 140, Color.GREEN);
        speedometer.addColoredRange(140, 380, Color.YELLOW);
        speedometer.addColoredRange(380, 500, Color.RED);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        //TODO Check weather the deice have light sensor or not
        if (mLight == null) {
            error.setVisibility(View.VISIBLE);
            ll_diff.setVisibility(View.GONE);
            textView_recommed_lux.setVisibility(View.GONE);
            error.setText(getResources().getString(string.no_sensor_msg));
            error.setTextColor(getResources().getColor(R.color.camera_msg_color));
//            textView_start.setVisibility(View.INVISIBLE);
            textView_digital_speedometer.setVisibility(View.GONE);
            lux_dect_next.setVisibility(View.VISIBLE);

        } else {
            isSpeedometerStarted = true;
//            textView_start.setText(string.stop);
            textView_digital_speedometer.setText("");
            stopSensor_auto();
        }



    }

    //TODO used to start stop sensor on button click event
    private void stopSensor_auto() {
        new Handler().postDelayed(new Runnable() {

            // TODO Using handler with postDelayed called runnable run method

            @Override
            public void run() {
                lux_dect_next.setVisibility(View.VISIBLE);
                ll_diff.setVisibility(View.VISIBLE);
                float difference_lux=MAX_LUX-lux;
                textview_room.setText(room_name+" light is ");
                if(difference_lux >(MAX_LUX*10)/100) {
                    textview_lux_diff.setTextColor(getResources().getColor(R.color.red_dark));
                    textview_lux_diff.setText("INSUFFICIENT ");
                }else{
                    textview_lux_diff.setTextColor(getResources().getColor(R.color.selected_background_green));
                    textview_lux_diff.setText("SUFFICIENT");
                }
                temp = lux;
                isSpeedometerStarted = false;
                stopSensor();
                //TODO set current lux to text view
                textView_digital_speedometer.setText(temp + " lx");
                //TODO calculate remaining lux of each room
            }
        }, 5000); // TODO wait for 5 seconds

    }

    //TODO method called on resume of activity
    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_NORMAL);
    }

    //TODO method called on pause of activity
    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    //TODO called every time if value of lux change
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.values[0]==0){
            lux = 1;
        }else{
            lux = sensorEvent.values[0];
        }
        if (isSpeedometerStarted) {
            speedometer.clearColoredRanges();
            speedometer.setSpeed(lux, 1000, 0);
            textView_digital_speedometer.setText("" + lux + " lx");
            speedometer.addColoredRange(0, 200, Color.GREEN);
            speedometer.addColoredRange(200, 300, Color.YELLOW);
            speedometer.addColoredRange(300, 500, Color.RED);

            if (lux <= 50) {
                speedometer.clearColoredRanges();
                speedometer.setMaxSpeed(50);
                speedometer.setMinSpeed(20);
                speedometer.setMajorTickStep(5);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 20, Color.GREEN);
                speedometer.addColoredRange(20, 30, Color.YELLOW);
                speedometer.addColoredRange(30, 50, Color.RED);
            } else if (lux >= 51 && lux <= 100) {
                speedometer.clearColoredRanges();
                speedometer.setMaxSpeed(100);
                speedometer.setMajorTickStep(10);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 40, Color.GREEN);
                speedometer.addColoredRange(40, 60, Color.YELLOW);
                speedometer.addColoredRange(60, 100, Color.RED);
            } else if (lux >= 101 && lux <= 300) {
                speedometer.clearColoredRanges();
                speedometer.setMaxSpeed(300);
                speedometer.setMajorTickStep(30);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 120, Color.GREEN);
                speedometer.addColoredRange(120, 180, Color.YELLOW);
                speedometer.addColoredRange(180, 300, Color.RED);
            } else if (lux >= 301 && lux <= 500) {
                speedometer.clearColoredRanges();
                speedometer.setMaxSpeed(500);
                speedometer.setMajorTickStep(50);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 200, Color.GREEN);
                speedometer.addColoredRange(200, 300, Color.YELLOW);
                speedometer.addColoredRange(300, 500, Color.RED);
            } else if (lux >= 501 && lux <= 800) {
                speedometer.clearColoredRanges();
                speedometer.setMaxSpeed(800);
                speedometer.setMajorTickStep(80);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 320, Color.GREEN);
                speedometer.addColoredRange(300, 460, Color.YELLOW);
                speedometer.addColoredRange(460, 800, Color.RED);
            } else if (lux >= 801 && lux <= 1000) {
                speedometer.clearColoredRanges();
                speedometer.setMaxSpeed(1000);
                speedometer.setMajorTickStep(100);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 400, Color.GREEN);
                speedometer.addColoredRange(400, 600, Color.YELLOW);
                speedometer.addColoredRange(600, 1000, Color.RED);
            } else if (lux >= 1001 && lux <= 3000) {
                speedometer.clearColoredRanges();
                speedometer.setMaxSpeed(3000);
                speedometer.setMajorTickStep(300);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 1200, Color.GREEN);
                speedometer.addColoredRange(1200, 1800, Color.YELLOW);
                speedometer.addColoredRange(1800, 3000, Color.RED);

            } else if (lux >= 3001 && lux <= 7000) {
                speedometer.clearColoredRanges();
                speedometer.setMaxSpeed(7000);
                speedometer.setMajorTickStep(700);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 2800, Color.GREEN);
                speedometer.addColoredRange(2800, 4200, Color.YELLOW);
                speedometer.addColoredRange(4200, 7000, Color.RED);
            } else if (lux >= 7001 && lux <= 10000) {
                speedometer.clearColoredRanges();
                speedometer.setMaxSpeed(10000);
                speedometer.setMajorTickStep(1000);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 4000, Color.GREEN);
                speedometer.addColoredRange(4000, 6000, Color.YELLOW);
                speedometer.addColoredRange(6000, 10000, Color.RED);
            } else if (lux >= 10001 && lux <= 200000) {

                speedometer.setMaxSpeed(200000);
                speedometer.setMajorTickStep(20000);
                speedometer.setMinorTicks(0);
                textView_digital_speedometer.setText("" + lux + " lx");
                speedometer.addColoredRange(0, 100000, Color.GREEN);
                speedometer.addColoredRange(100000, 150000, Color.YELLOW);
                speedometer.addColoredRange(150000, 200000, Color.RED);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    //TODO method use to start media when sensor stop
    public void stopSensor() {
        mediaPlayer.start();
    }
}
