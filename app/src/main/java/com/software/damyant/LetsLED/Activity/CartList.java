package com.software.damyant.LetsLED.Activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.software.damyant.LetsLED.CartListDataBase;
import com.software.damyant.LetsLED.R;
import com.software.damyant.LetsLED.Adapter.CartAdapter;

import java.util.HashMap;


/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class used for cart activity
 */
public class CartList extends Activity {

    private Button button_summary_buton;
    private ListView cart_detail_list;
    private String[] cart_url;
    private String[] cart_name;
    private String[] val;
    private String[] cart_id;
    private String[] cart_price;
    private int[] cart_quanity;
    private HashMap<String, String[]> cart_list_value = new HashMap<String, String[]>();
    private CartAdapter cart_adapter;
    private TextView textView;
    private ImageView imageView4;
    private double grand_total_view;
    private CartListDataBase cartListDataBase = MainActivity.cartdata;

    //TODO  Called when the activity is first created.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_list);
        textView = (TextView) findViewById(R.id.textview_total_price);
        button_summary_buton = (Button) findViewById(R.id.button_summary_buton);
        imageView4= (ImageView) findViewById(R.id.imageView4);
        //TODO get all value from database
        cart_list_value = cartListDataBase.getAllCart();
        //TODO initialise all arrays
        cart_url = new String[cart_list_value.size()];
        cart_name = new String[cart_list_value.size()];
        cart_id = new String[cart_list_value.size()];
        cart_price = new String[cart_list_value.size()];
        cart_quanity= new int[cart_list_value.size()];
        //TODO put values from cart database
        for (int i = 0; i < cart_list_value.size(); i++) {
            val = (String[]) cart_list_value.get("" + i);
            cart_url[i] = val[2];
            cart_name[i] = val[0];
            cart_price[i] = val[1];
            cart_id[i] = val[3];
            cart_quanity[i]=Integer.parseInt(val[4]);
        }

        //TODO initialise cart adapter and set adopter to list
        cart_detail_list = (ListView) findViewById(R.id.listview_selected_item);
        cart_adapter = new CartAdapter(CartList.this, cart_url, cart_name, cart_id, cart_price,cart_quanity);
        cart_detail_list.setAdapter(cart_adapter);

        //TODO on click of proceed to payment navigate to UserDetail activity
        button_summary_buton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent payment = new Intent(CartList.this, UserDetail.class);
                startActivity(payment);
            }
        });
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(CartList.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

            }
        });
        updateGrandTotal();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    //TODO use to update Grand total value
    public void updateGrandTotal() {
        grand_total_view = 0;
        for (int i = 0; i < cart_list_value.size(); i++) {

            val = (String[]) cart_list_value.get("" + i);
            cart_price[i] = val[1];
            Cursor cursor = cartListDataBase.getQunity(Integer.parseInt(cart_id[i]));
            int quantity = 0;
            if (cursor.moveToFirst()) {
                quantity = Integer.parseInt(cursor.getString(0));
            }
            grand_total_view += Integer.parseInt(cart_price[i]) * quantity;

        }
        textView.setText("  " + grand_total_view);
    }
}
