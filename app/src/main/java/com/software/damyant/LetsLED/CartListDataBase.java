package com.software.damyant.LetsLED;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;

/**
 * Created by sl1 on 8/6/15.
 */
public class CartListDataBase extends SQLiteOpenHelper {


    public CartListDataBase(Context context) {
        super(context, AppConstant.DATABASE_NAME, null, AppConstant.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.TABLE_NAME +
                        "(" + AppConstant.PRODUCT_ID + " integer primary key, " + AppConstant.PRODUCT_NAME + " text," + AppConstant.PRODUCT_PRICE + " integer," + AppConstant.PRODUCT_IMAGE_URL + " text," + AppConstant.PRODUCT_QUANTITY + " integer," + AppConstant.PRODUCT_SELECTED + " integer)"
        );
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + AppConstant.ROOM_TABLE_NAME +
                        "(" + AppConstant.ROOM_ID + " integer primary key, " + AppConstant.ROOM_TYPE + " text," + AppConstant.ROOM_LUX + " integer)"
        );

        sqLiteDatabase.execSQL("INSERT INTO " + AppConstant.ROOM_TABLE_NAME + " VALUES('1','Living Room','200');");
        sqLiteDatabase.execSQL("INSERT INTO " + AppConstant.ROOM_TABLE_NAME + " VALUES('2','Bedroom','300');");
        sqLiteDatabase.execSQL("INSERT INTO " + AppConstant.ROOM_TABLE_NAME + " VALUES('3','Kitchen','250');");
        sqLiteDatabase.execSQL("INSERT INTO " + AppConstant.ROOM_TABLE_NAME + " VALUES('4','Dining Room','200');");
        sqLiteDatabase.execSQL("INSERT INTO " + AppConstant.ROOM_TABLE_NAME + " VALUES('5','Office','400');");
        sqLiteDatabase.execSQL("INSERT INTO " + AppConstant.ROOM_TABLE_NAME + " VALUES('6','Industry','750');");
        sqLiteDatabase.execSQL("INSERT INTO " + AppConstant.ROOM_TABLE_NAME + " VALUES('7','Shop','400');");
        sqLiteDatabase.execSQL("INSERT INTO " + AppConstant.ROOM_TABLE_NAME + " VALUES('8','Bathroom','200');");
        sqLiteDatabase.execSQL("INSERT INTO " + AppConstant.ROOM_TABLE_NAME + " VALUES('9','Study Room','500');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }
    //TODO Use to get room lux from lightfinderDB table
    public Cursor getRoomLux(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + AppConstant.ROOM_TABLE_NAME + " WHERE " + AppConstant.ROOM_ID + "=" + id, null);
        return res;
    }
    //TODO Use to insert data in cartlist table
    public boolean insertData(int id, String name, float price, String url, int quanity, int selected) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.PRODUCT_ID, id);
        contentValues.put(AppConstant.PRODUCT_PRICE, price);
        contentValues.put(AppConstant.PRODUCT_NAME, name);
        contentValues.put(AppConstant.PRODUCT_IMAGE_URL, url);
        contentValues.put(AppConstant.PRODUCT_QUANTITY, quanity);
        contentValues.put(AppConstant.PRODUCT_SELECTED, selected);
        db.insert(AppConstant.TABLE_NAME, null, contentValues);
        return true;
    }
    //TODO Use to get Quantity column from catlist table
    public Cursor getQunity(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select " + AppConstant.PRODUCT_QUANTITY + " from " + AppConstant.TABLE_NAME + " where " + AppConstant.PRODUCT_ID + "=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, "cartlist");
        return numRows;
    }
    //TODO Use to update Quantity column from catlist table
    public void updateQuanity(Integer id, Integer Quanity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.PRODUCT_QUANTITY, Quanity);
        db.update(AppConstant.TABLE_NAME, contentValues, AppConstant.PRODUCT_ID + "= ? ", new String[]{Integer.toString(id)});
    }
    //TODO Use to get selected column from catlist table
    public Cursor getSelected(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select " + AppConstant.PRODUCT_SELECTED + " from " + AppConstant.TABLE_NAME + " where " + AppConstant.PRODUCT_ID + "=" + id + "", null);
        return res;
    }
    //TODO Use to update selected column from catlist table
    public void updateSelected(Integer id, Integer selected) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppConstant.PRODUCT_SELECTED, selected);
        db.update(AppConstant.TABLE_NAME, contentValues, AppConstant.PRODUCT_ID + " = ? ", new String[]{Integer.toString(id)});
    }

    //TODO Use to delet one record from catlist table
    public Integer deleteProduct(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(AppConstant.TABLE_NAME,
                AppConstant.PRODUCT_ID + "= ? ",
                new String[]{Integer.toString(id)});
    }

    //TODO use to get all data from catlist table
    public HashMap<String, String[]> getAllCart() {
        HashMap<String, String[]> map1 = new HashMap<String, String[]>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + AppConstant.TABLE_NAME, null);
        int i = 0;
        if (res.moveToFirst()) {
            do {
                String[] cartProduct = new String[6];
                cartProduct[0] = res.getString(1);
                cartProduct[1] = res.getString(2);
                cartProduct[2] = res.getString(3);
                cartProduct[3] = res.getString(0);
                cartProduct[4] = res.getString(4);
                cartProduct[5] = res.getString(5);
                map1.put(Integer.toString(i), cartProduct);
                i++;
            } while (res.moveToNext());
        }

        return map1;
    }

    //TODO closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
}
