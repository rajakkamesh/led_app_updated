package com.software.damyant.LetsLED.Camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.software.damyant.LetsLED.Activity.ColorDetectorScreen;
import com.software.damyant.LetsLED.AppConstant;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class used in Color Detor class
 */
public class ShowCamera extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {

    private SurfaceHolder holdMe;
    private Camera theCamera;
    private Context context;
    private View  wall_color, furniture_color;
    boolean previewIsRunning = false;
    public static boolean isWhite;

    //TODO Constructor of show camera class
    public ShowCamera(Context context, View view2, View view3) {
        super(context);
        this.context = context;
        holdMe = getHolder();
        holdMe.addCallback(this);
        this.wall_color = view2;
        this.furniture_color = view3;
    }
    //TODO  Called when the activity is first created.
    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        try {
            theCamera.setPreviewDisplay(arg0);
            theCamera.setPreviewCallback(this);
            myStartPreview();

        } catch (Exception e) {

        }


    }
    //TODO method use to start preview of camera
    public void myStartPreview() {
        if (!previewIsRunning && (theCamera != null)) {
            try {
                theCamera.startPreview();
                Camera.Parameters params = theCamera.getParameters();
                if (params.getSupportedFocusModes().contains(
                        Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                }
                theCamera.setParameters(params);
            } catch (Exception e) {
            }
            previewIsRunning = true;
        }
    }

    //TODO called on surface created camera is open in this method
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (theCamera == null) {
            theCamera = Camera.open();
            theCamera.setDisplayOrientation(90);//TODO orientation of camera is change with 90 degree
        }
    }

    //TODO called on surface destroy
    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        myStopPreview();
        theCamera.release();
        theCamera = null;
    }

    //TODO called when camera preview stop
    public void myStopPreview() {
        if (previewIsRunning && (theCamera != null)) {
            theCamera.stopPreview();
            previewIsRunning = false;
        }
    }
    //TODO USE to show color on preview
    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        try {
            int frameHeight = camera.getParameters().getPreviewSize().height;
            int frameWidth = camera.getParameters().getPreviewSize().width;
            // TODO number of pixels//transforms NV21 pixel data into RGB pixels
            int rgb[] = new int[frameWidth * frameHeight];
            // TODO convertion
            decodeYUV420SP(rgb, data, frameWidth, frameHeight);
            Bitmap bmp = Bitmap.createBitmap(rgb, frameWidth, frameHeight, Config.ARGB_8888);
            int pixel = bmp.getPixel(bmp.getWidth()/2,bmp.getHeight()/2);
            int redValue = Color.red(pixel);
            int blueValue = Color.blue(pixel);
            int greenValue = Color.green(pixel);
            int thiscolor = Color.rgb(redValue, greenValue, blueValue);
            if (ColorDetectorScreen.click_num == 0) {
                wall_color.setBackgroundColor(thiscolor);
                int hexcode=Integer.parseInt(decToHex(thiscolor));
//                Toast.makeText(context,"color coe="+hexcode,Toast.LENGTH_SHORT).show();
//                System.out.println("color coe="+hexcode);
                if(hexcode < AppConstant.COLOR_CODE) {
                    isWhite=true;
                }else{
                    isWhite=false;
                }
            } else if (ColorDetectorScreen.click_num == 1) {
                furniture_color.setBackgroundColor(thiscolor);
            }
        }catch (Exception e){

        }
    }

    //TODO method used to find integer color code from camera preview
    static public void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width, int height) {
        try {
            final int frameSize = width * height;
            for (int j = 0, yp = 0; j < height; j++) {
                int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
                for (int i = 0; i < width; i++, yp++) {
                    int y = (0xff & ((int) yuv420sp[yp])) - 16;
                    if (y < 0) y = 0;
                    if ((i & 1) == 0) {
                        v = (0xff & yuv420sp[uvp++]) - 128;
                        u = (0xff & yuv420sp[uvp++]) - 128;
                    }
                    int y1192 = 1192 * y;
                    int r = (y1192 + 1634 * v);
                    int g = (y1192 - 833 * v - 400 * u);
                    int b = (y1192 + 2066 * u);

                    if (r < 0) r = 0;
                    else if (r > 262143) r = 262143;
                    if (g < 0) g = 0;
                    else if (g > 262143) g = 262143;
                    if (b < 0) b = 0;
                    else if (b > 262143) b = 262143;

                    rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);

                }
            }
        }catch (Exception e){

        }
    }


    private static final int sizeOfIntInHalfBytes = 6;
    private static final int numberOfBitsInAHalfByte = 4;
    private static final int halfByte = 0x0F;
    private static final char[] hexDigits = {
            '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    // TODO methods use to convert integer color code to hexa-decimal
    public static String decToHex(int dec) {
        StringBuilder hexBuilder = new StringBuilder(sizeOfIntInHalfBytes);
        hexBuilder.setLength(sizeOfIntInHalfBytes);
        for (int i = sizeOfIntInHalfBytes - 1; i >= 0; --i) {
            int j = dec & halfByte;
            hexBuilder.setCharAt(i, hexDigits[j]);
            dec >>= numberOfBitsInAHalfByte;
        }
        return hexBuilder.toString();
    }


}

