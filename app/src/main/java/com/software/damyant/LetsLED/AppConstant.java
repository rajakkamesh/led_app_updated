package com.software.damyant.LetsLED;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by sl1 on 30/5/15.
 */
public class AppConstant {

    // URL to get contacts JSON
    public static String url = "http://sltdev.com/client/moser/api/web/v1/products/product-list";
    // JSON Node names
    public static String img_url = "http://sltdev.com";
    public static final String PRODUCT_LIST = "product_list";
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String PRODUCT_DESCRIPTION = "product_description";
    public static final String PRODUCT_STATUS = "product_status";
    public static final String PRODUCT_PRICE = "product_price";
    public static final String PRODUCT_IMAGE_URL = "product_image_url";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "cartlist.db";
    public static final String TABLE_NAME ="cartlist";

    public static final String PRODUCT_QUANTITY = "product_quantity";
    public static final String PRODUCT_SELECTED= "product_selected";

    public static final String ROOM_TABLE_NAME ="lightfinderDB";
    public static final String ROOM_ID = "room_id";
    public static final String ROOM_TYPE = "room_type";
    public static final String ROOM_LUX= "romm_lux";

    public static final int time_to_splash_resume= 1000;

    public static final int TOTAL_NUMBER_ROOM= 9;
    public static final int CART_PRODUCT_LIMIT= 1000;

    public static final int COLOR_CODE=524235;

    public static final String landing_screen_text = String.valueOf(Html
            .fromHtml("<![CDATA[<body style=\"text-align:justify;background-color:#00222222;\">"
                    + "Lighting is one of the key elements that make your house a home. It enhances your room ambiance, dramatizes wall textures, artworks and furnishings. Both at Office or home, efficient lighting enables you to do work with ease, besides making it safer and comfortable. Each room has specific and unique lighting needs. Not choosing the Right color of light can totally mar your decor as color enhances the ambiance and mood vital for health. Moserbaer presents Let&#39;s LED, know how for planning your lighting needs."+
    "Lighting has two key properties, Color of light from white to yellow (measured as CCT) and Lumens output (amount of light emitted by lighting product). Higher the lumens per watt of power the more efficient the lighting. The third key feature of Light source is the lux. It tells the amount of light falling on your task area and shows how efficiently lighting device directs the light to desired area. Moserbaer High technology lights are higher lumens at lower power wattage and give high lux."+
                    "</body>]]>"));

    //TODO if user click on details button on room then show info about room
    public static final void showDialogs(Context context,int i) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.home_layout);
        TextView title = (TextView) dialog.findViewById(R.id.textview_alert_title);

        if(i==0) {
            dialog.setTitle("How to use?");
            title.setText("How to use?");
        }else{
            dialog.setTitle("Disclaimer");
            title.setText("Disclaimer");
        }
        //TODO set the custom dialog components - text, image and button
        WebView alert_web = (WebView) dialog.findViewById(R.id.textview_alert_detail);
        String youtContentStr=null;
        if(i==0) {
            youtContentStr = String.valueOf(Html
                    .fromHtml("<![CDATA[<body style=\"text-align:justify;background-color:#00222222;\"><li>Focus your camera in the view box and click to detect color of wall</li><br><li>Focus your camera in the view box and click to detect color of furniture</li><br><li>Wait ...Let&#39;s LED  is working for you </li><br><li>Follow the guidelines of Let&#39;s LED to enjoy better home, office and shop</li><br><li>You like it share with others ...Let&#39;s LED</li></body>]]>"));
        }else{
            youtContentStr = String.valueOf(Html
                    .fromHtml("<![CDATA[<body style=\"text-align:justify;background-color:#00222222;\"><li>Recommendations of this application are only indicative in nature.</li><br><li>Measured values are dependent on resolution of sensors used in your mobile device.</li><br><li>Moserbaer cannot be held responsible for any discrepancy with regards to the recommendation. </li></body>]]>"));
        }

        alert_web.setBackgroundColor(Color.TRANSPARENT);
        WebSettings webSettings = alert_web.getSettings();
        webSettings.setDefaultFontSize(15);
        alert_web.loadData(youtContentStr, "text/html", "utf-8");
        dialog.setCanceledOnTouchOutside(false);
        ImageView dialogButton = (ImageView) dialog.findViewById(R.id.button_alert_ok);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
