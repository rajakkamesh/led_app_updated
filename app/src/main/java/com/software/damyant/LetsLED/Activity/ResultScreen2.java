package com.software.damyant.LetsLED.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.software.damyant.LetsLED.AppConstant;
import com.software.damyant.LetsLED.Camera.ShowCamera;
import com.software.damyant.LetsLED.R;
import com.software.damyant.LetsLED.Service.ConnectionDetector;
import java.text.DecimalFormat;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class use to show the result
 */
public class ResultScreen2 extends Activity {
    private Button submit, button_change,button_enquiry;
    private TextView new_power_txt, new_ene_txt, new_cost_txt;
    private TextView exist_power_txt, exist_ene_txt, exist_cost_txt;
    private TextView save_money_txt, assumed_cost,room_name_txt;
    private ImageView existing_imageview, new_imageview, imageView4;
    private Context context;
    private View asth_val;
    private LinearLayout ll_home;
    private TextView color_name;
    Boolean isInternetPresent = false;
    private float cost = 7.50f;
    private int[] cfl={8,11,15,18,9,20,22,23};
    private int[] led_cfl={5,5,7,7,5,9,9,9};
    private int[] bulb={25,40,60,100};
    private int[] led_bulb={5,7,9,12};
    private int[] tubelight_2ft={14,18,20,24,28};
    private int[] led_tubelight_2ft={8,8,8,8,15};
    private int[] tubelight_4ft={36,40,54,65};
    private int[] led_tubelight_4ft={15,15,22,22};
    private int[] shop_cfl={8,9,11,15,18,20,22,24,26,30,36};
    private int[] shope_led={6,6,9,9,9,9,12,12,12,15,18};
    private int[] industry_cfl={70,150,250,400};
    private int[] industry_led={30,65,90,150};
    ConnectionDetector cd = new ConnectionDetector(this);

    /**
     * TODO  Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.result_screen3);
        exist_power_txt = (TextView) findViewById(R.id.existing_pow_txt);
        exist_ene_txt = (TextView) findViewById(R.id.existing_ene_txt);
        exist_cost_txt = (TextView) findViewById(R.id.existing_cost_txt);
        new_power_txt = (TextView) findViewById(R.id.new_pow_txt);
        new_ene_txt = (TextView) findViewById(R.id.new_ene_txt);
        new_cost_txt = (TextView) findViewById(R.id.new_cost_txt);
        save_money_txt = (TextView) findViewById(R.id.textView_save);
        assumed_cost = (TextView) findViewById(R.id.assumed_cost);
        room_name_txt= (TextView) findViewById(R.id.room_name_txt);
        color_name= (TextView) findViewById(R.id.color_txt);
        ll_home=(LinearLayout)findViewById(R.id.ll_home);
        existing_imageview = (ImageView) findViewById(R.id.existing_imageview);
        new_imageview = (ImageView) findViewById(R.id.new_imageview);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        submit = (Button) findViewById(R.id.button_start);
        button_change = (Button) findViewById(R.id.button_change);
        button_enquiry= (Button) findViewById(R.id.button_enquiry);
        asth_val=findViewById(R.id.asth_val);
        room_name_txt.setText("("+DetectLuxActivity.room_name+")");
        for (int i = 1; i <= AppConstant.TOTAL_NUMBER_ROOM; i++) {
            if (Room_Chooser.num == 10) {
                if (Home_Room_Chooser.num == i) {
                    if(i!=1&& i!=2 && i!=4){
                        asth_val.setBackgroundColor(getResources().getColor(R.color.white));
                        color_name.setText("Cool White");
                    }else{
                        if(ShowCamera.isWhite){
                            asth_val.setBackgroundColor(getResources().getColor(R.color.white));
                            color_name.setText("Cool White");
                        }else {
                            asth_val.setBackgroundColor(getResources().getColor(R.color.yellow_color));
                            color_name.setText("Warm White");
                        }
                    }
                }
            } else {
                if (Room_Chooser.num == i) {
                    if(i==7){
                        if(ShowCamera.isWhite){
                            color_name.setText("Cool White");
                            asth_val.setBackgroundColor(getResources().getColor(R.color.white));
                        }else {
                            color_name.setText("Warm White");
                            asth_val.setBackgroundColor(getResources().getColor(R.color.yellow_color));
                        }
                    }else{
                        color_name.setText("Cool White");
                        asth_val.setBackgroundColor(getResources().getColor(R.color.white));
                    }
                }

            }
        }


        //TODO called when click on optimise setting
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO check for internet connection
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    //TODO activity navigate to Product list if internet present
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://shop.moserbaer.in/51-pcat-LED-Product-LED-Lighting"));
                        startActivity(browserIntent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(context, "No application can handle this request."
                                + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(ResultScreen2.this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                }
            }
        });

        button_enquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO check for internet connection
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    //TODO activity navigate to Product list if internet present
                    Intent i = new Intent(ResultScreen2.this, UserDetail.class);
                    startActivity(i);

                } else {
                    Toast.makeText(ResultScreen2.this, getResources().getString(R.string.no_internet_msg), Toast.LENGTH_LONG).show();
                }
            }
        });
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(ResultScreen2.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

            }
        });

        //TODO called when click on optimise setting
        ll_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(ResultScreen2.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

        //TODO called when click on optimise setting
        button_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }
        });
        calculate();

    }

    public void calculate() {
        try {
            String type_light = ValueInsert.type_light;
            int num_Light = Integer.parseInt(ValueInsert.num_Light);
            int watt_light = Integer.parseInt(ValueInsert.Watt_light);
            exist_power_txt.setText("" + watt_light);
            float exist_energy = (watt_light * (Integer.parseInt(ValueInsert.num_days) * Integer.parseInt(ValueInsert.num_hours)) * 52);
            float exist_energy_kwh = (exist_energy / 1000) * num_Light;
            DecimalFormat df = new DecimalFormat("#.##");
            exist_ene_txt.setText("" + df.format(exist_energy_kwh));
            double exist_cost_kwh = exist_energy_kwh * cost;
            exist_cost_txt.setText("" + df.format(exist_cost_kwh));
            if (type_light.equals(getResources().getString(R.string.cfl))) {
                int eualvalent_cfl_energy = CFL_to_LED(watt_light);
                new_power_txt.setText("" + eualvalent_cfl_energy);
                float newenergy = (eualvalent_cfl_energy * (Integer.parseInt(ValueInsert.num_days) * Integer.parseInt(ValueInsert.num_hours)) * 52);
                float new_energy_kwh = (newenergy / 1000) * num_Light;
                new_ene_txt.setText("" + df.format(new_energy_kwh));
                new_cost_txt.setText("" + df.format(new_energy_kwh * cost));
                existing_imageview.setImageDrawable(getResources().getDrawable(R.drawable.cfl));
                new_imageview.setImageDrawable(getResources().getDrawable(R.drawable.ledbulb));
            } else if (type_light.equals(getResources().getString(R.string.blub))) {
                float eualvalent_bulb_energy = BULB_to_LED(watt_light);
                new_power_txt.setText("" + eualvalent_bulb_energy);
                float newenergy = (eualvalent_bulb_energy * (Integer.parseInt(ValueInsert.num_days) * Integer.parseInt(ValueInsert.num_hours)) * 52);
                float new_energy_kwh = (newenergy / 1000) * num_Light;
                new_ene_txt.setText("" + df.format(new_energy_kwh));
                new_cost_txt.setText("" + df.format(new_energy_kwh * cost));
                existing_imageview.setImageDrawable(getResources().getDrawable(R.drawable.bulb));
                new_imageview.setImageDrawable(getResources().getDrawable(R.drawable.ledbulb));
            }else if ( type_light.equals("2 FT TUBELIGHT")) {
                float eualvalent_tubelight_energy = TUBELIGHT2ft_to_LED(watt_light);
                new_power_txt.setText("" +df.format(eualvalent_tubelight_energy));
                float newenergy = (eualvalent_tubelight_energy * (Integer.parseInt(ValueInsert.num_days) * Integer.parseInt(ValueInsert.num_hours)) * 52);
                float new_energy_kwh = (newenergy / 1000) * num_Light;
                new_ene_txt.setText("" + df.format(new_energy_kwh));
                new_cost_txt.setText("" + df.format(new_energy_kwh * cost));
                existing_imageview.setImageDrawable(getResources().getDrawable(R.drawable.tubelight));
                new_imageview.setImageDrawable(getResources().getDrawable(R.drawable.ledtubelight));
            } else if (type_light.equals("4 FT TUBELIGHT")) {
                float eualvalent_tubelight_energy = TUBELIGHT_to_LED(watt_light);
                new_power_txt.setText("" + eualvalent_tubelight_energy);
                float newenergy = (eualvalent_tubelight_energy * (Integer.parseInt(ValueInsert.num_days) * Integer.parseInt(ValueInsert.num_hours)) * 52);
                float new_energy_kwh = (newenergy / 1000) * num_Light;
                new_ene_txt.setText("" + df.format(new_energy_kwh));
                new_cost_txt.setText("" + df.format(new_energy_kwh * cost));
                existing_imageview.setImageDrawable(getResources().getDrawable(R.drawable.tubelight));
                new_imageview.setImageDrawable(getResources().getDrawable(R.drawable.ledtubelight));
            }else if(type_light.equals("Double Lamp DL")||type_light.equals("Single Lamp DL")){
                float eualvalent_cfl_energy = SHOPLIGHT_to_LED(watt_light);
                new_power_txt.setText("" +eualvalent_cfl_energy);
                float newenergy = (eualvalent_cfl_energy * (Integer.parseInt(ValueInsert.num_days) * Integer.parseInt(ValueInsert.num_hours)) * 52);
                float new_energy_kwh = (newenergy / 1000) * num_Light;
                new_ene_txt.setText("" + df.format(new_energy_kwh));
                new_cost_txt.setText("" + df.format(new_energy_kwh * cost));
                existing_imageview.setImageDrawable(getResources().getDrawable(R.drawable.cfl));
                new_imageview.setImageDrawable(getResources().getDrawable(R.drawable.ledbulb));
            }else if(type_light.equals("HPSV")||type_light.equals("MH")){
                float eualvalent_bulb_energy = INDUSTRYLIGHT_to_LED(watt_light);
                new_power_txt.setText("" + eualvalent_bulb_energy);
                float newenergy = (eualvalent_bulb_energy * (Integer.parseInt(ValueInsert.num_days) * Integer.parseInt(ValueInsert.num_hours)) * 52);
                float new_energy_kwh = (newenergy / 1000) * num_Light;
                new_ene_txt.setText("" + df.format(new_energy_kwh));
                new_cost_txt.setText("" + df.format(new_energy_kwh * cost));
                existing_imageview.setImageDrawable(getResources().getDrawable(R.drawable.bulb));
                new_imageview.setImageDrawable(getResources().getDrawable(R.drawable.ledbulb));
            }
            save_money_txt.setText("" + df.format(exist_cost_kwh - Float.parseFloat(new_cost_txt.getText().toString())));
        }catch (Exception e){

        }
    }

    //TODO method use to show cost input dialog on result screen when click on change button
    public void showDialog() {

        final Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.msg_dialog);
        Button button_ok = (Button) builder.findViewById(R.id.button_ok);
        Button button_cancel = (Button) builder.findViewById(R.id.button_cancel);
        final EditText cost_edittxt = (EditText) builder.findViewById(R.id.cost_txtview);

        cost_edittxt.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(cost_edittxt, 0);
            }
        }, 20);

        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cost_edittxt.getText().toString().isEmpty()) {
                    cost = Float.parseFloat(cost_edittxt.getText().toString());
                    assumed_cost.setText("Energy cost assumed at Rs. " + cost + " per kWh");
                    calculate();
                }
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(),0);
                builder.cancel();
            }
        });
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(),0);
                builder.cancel();
            }
        });
        builder.show();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    //TODO method use to convert Bulb into led equivalent watt
    public int BULB_to_LED(int watt) {
        for(int i=0;i<bulb.length;i++) {
            if(bulb[i]==watt) {
                return led_bulb[i];
            }
        }
        return 0;
    }

    //TODO method use to convert CFL into led equivalent watt
    public int CFL_to_LED(int watt) {
        for(int i=0;i<cfl.length;i++) {
            if(cfl[i]==watt) {
                return led_cfl[i];
            }
        }
        return 0;
    }
    //TODO method use to convert tubelight into led equivalent watt
    public int TUBELIGHT2ft_to_LED(int watt) {
        for(int i=0;i<tubelight_2ft.length;i++) {
            if(tubelight_2ft[i]==watt) {
                return led_tubelight_2ft[i];
            }
        }
        return 0;
    }

    //TODO method use to convert tubelight into led equivalent watt
    public float TUBELIGHT_to_LED(int watt) {
        for(int i=0;i<tubelight_4ft.length;i++) {
            if(tubelight_4ft[i]==watt) {
                return led_tubelight_4ft[i];
            }
        }
        return 0;
    }

    //TODO method use to convert tubelight into led equivalent watt
    public int INDUSTRYLIGHT_to_LED(int watt) {
        for(int i=0;i<industry_cfl.length;i++) {
            if(industry_cfl[i]==watt) {
                return industry_led[i];
            }
        }
        return 0;
    }

    //TODO method use to convert tubelight into led equivalent watt
    public float SHOPLIGHT_to_LED(int watt) {
        for(int i=0;i<shop_cfl.length;i++) {
            if(shop_cfl[i]==watt) {
                return shope_led[i];
            }
        }
        return 0;
    }
}
