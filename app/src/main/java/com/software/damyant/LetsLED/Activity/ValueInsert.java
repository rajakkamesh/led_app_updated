package com.software.damyant.LetsLED.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.software.damyant.LetsLED.R;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class use to take input from users
 */
public class ValueInsert extends Activity implements AdapterView.OnItemSelectedListener, View.OnFocusChangeListener {
    private Button calculate;
    private ImageView imageView4;
    public static String type_light, num_Light, Watt_light, num_days, num_hours;
    private Spinner type_light_spin, watt_light_spin;
    private EditText num_light_edit, num_days_edit, num_hours_edit;
    private LinearLayout ll_home, ll_wattage;
    private Context context;


    //TODO  Called when the activity is first created.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.value_insert);
        context = this;
        this.getWindow().getDecorView().clearFocus();
        calculate = (Button) findViewById(R.id.button_calculate);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        num_light_edit = (EditText) findViewById(R.id.num_light_edittext);
        num_light_edit.setFilters(new InputFilter[]{new InputFilterMinMax("1", "100")});
        num_days_edit = (EditText) findViewById(R.id.num_day_edittext);
        num_days_edit.setFilters(new InputFilter[]{new InputFilterMinMax("1", "7")});
        num_hours_edit = (EditText) findViewById(R.id.num_hours_edittext);
        num_hours_edit.setFilters(new InputFilter[]{new InputFilterMinMax("1", "24")});
        type_light_spin = (Spinner) findViewById(R.id.type_light_spin);
        watt_light_spin = (Spinner) findViewById(R.id.watt_light_spin);
        ll_home = (LinearLayout) findViewById(R.id.ll_home);
        ll_wattage = (LinearLayout) findViewById(R.id.ll_wattage);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        if (Room_Chooser.num == 7) {
            String[] yourArray = getResources().getStringArray(R.array.light_type_array_shop);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, yourArray);
            type_light_spin.setAdapter(dataAdapter);
        } else if (Room_Chooser.num == 6) {
            String[] yourArray = getResources().getStringArray(R.array.light_type_array_industry);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, yourArray);
            String[] yourArray2 = getResources().getStringArray(R.array.industry_watts);
            ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, yourArray2);
            watt_light_spin.setAdapter(dataAdapter2);
            type_light_spin.setAdapter(dataAdapter);
        }

        //TODO  use to check weather all the input are selected or not

        if (type_light_spin.getSelectedItem().equals(getResources().getString(R.string.not_select_spinner)) || watt_light_spin.getSelectedItem().equals(getResources().getString(R.string.not_select_spinner))) {
            calculate.setVisibility(View.GONE);
        } else {
            calculate.setVisibility(View.VISIBLE);
        }

        //TODO  if button click then it will navigate to Lux detection activity

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ValueInsert.this, DetectLuxActivity.class);
                startActivity(i);
            }
        });
        //TODO  if button click then it will navigate to Lux detection activity

        ll_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(ValueInsert.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

        //TODO  if home icon click then it will navigate to Lux detection activity
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(ValueInsert.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);


            }
        });

        //TODO  set Item listener on all spinners

        type_light_spin.setOnItemSelectedListener(this);
        watt_light_spin.setOnItemSelectedListener(this);
        num_light_edit.setOnFocusChangeListener(this);
        num_days_edit.setOnFocusChangeListener(this);
        num_hours_edit.setOnFocusChangeListener(this);

    }

    String[] watt_array;
    ArrayAdapter<String> wattAdapter;

    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {

        //TODO  set value to variable which are not selected

        num_light_edit.clearFocus();
        num_days_edit.clearFocus();
        num_hours_edit.clearFocus();

        num_Light = num_light_edit.getText().toString();
        type_light = type_light_spin.getSelectedItem().toString();
        Watt_light = watt_light_spin.getSelectedItem().toString();
        num_hours = num_hours_edit.getText().toString();
        num_days = num_days_edit.getText().toString();
        if(parent.getId()==R.id.type_light_spin) {
            switch (type_light) {
                case "CFL":
                    watt_array = getResources().getStringArray(R.array.cfl_watts);
                    wattAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, watt_array);
                    watt_light_spin.setAdapter(wattAdapter);
                    break;
                case "BULB":
                    watt_array = getResources().getStringArray(R.array.bulb_watts);
                    wattAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, watt_array);
                    watt_light_spin.setAdapter(wattAdapter);
                    break;
                case "2 FT TUBELIGHT":
                        watt_array = getResources().getStringArray(R.array.FT2_tubelight_watts);
                    wattAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, watt_array);
                    watt_light_spin.setAdapter(wattAdapter);
                    break;
                case "4 FT TUBELIGHT":
                    watt_array = getResources().getStringArray(R.array.tubelight_watts);
                    wattAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, watt_array);
                    watt_light_spin.setAdapter(wattAdapter);
                    break;
                case ("Single Lamp DL"):
                case ("Double Lamp DL"):
                    watt_array = getResources().getStringArray(R.array.shop_watts);
                    wattAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, watt_array);
                    watt_light_spin.setAdapter(wattAdapter);
                    break;
                default:
                     // they are executed if none of the above case is satisfied
                    break;
            }
            if (!type_light_spin.getSelectedItem().equals(getResources().getString(R.string.not_select_spinner))) {
                ll_wattage.setVisibility(View.VISIBLE);
            }
        }
        if (num_hours.equals("") || num_days.equals("") || num_Light.equals("") || type_light_spin.getSelectedItem().equals(getResources().getString(R.string.not_select_spinner)) || watt_light_spin.getSelectedItem().equals(getResources().getString(R.string.not_select_spinner))) {
            calculate.setVisibility(View.GONE);
        } else {
            calculate.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    protected void onResume() {
        this.getWindow().getDecorView().clearFocus();
        super.onResume();

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if ((v.getId() == R.id.num_light_edittext || v.getId() == R.id.num_hours_edittext || v.getId() == R.id.num_day_edittext) && !hasFocus) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        }
    }
}
