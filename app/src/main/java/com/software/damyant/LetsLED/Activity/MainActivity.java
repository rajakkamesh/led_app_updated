package com.software.damyant.LetsLED.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;

import com.software.damyant.LetsLED.AppConstant;
import com.software.damyant.LetsLED.CartListDataBase;
import com.software.damyant.LetsLED.R;

/**
 *TODO  First activity to show splash screen
 */
public class MainActivity extends Activity {
    public static CartListDataBase cartdata;
    //TODO  Called when the activity is first created.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cartdata=new CartListDataBase(this);
        new Handler().postDelayed(new Runnable() {

            // TODO Using handler with postDelayed called runnable run method

            @Override
            public void run() {
                // TODO navigate to Landing screen activity
                Intent i = new Intent(MainActivity.this, LandingScreen.class);
                startActivity(i);

                // TODO close this activity
                finish();
            }
        }, AppConstant.time_to_splash_resume); // TODO wait for 2.5 seconds

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}