package com.software.damyant.LetsLED.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.software.damyant.LetsLED.AppConstant;
import com.software.damyant.LetsLED.CartListDataBase;
import com.software.damyant.LetsLED.R;
import com.software.damyant.LetsLED.Service.ServiceHandler;
import com.software.damyant.LetsLED.Adapter.LazyAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.HashMap;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class use to show product list
 */
public class ProductList extends Activity {

    public ProgressDialog pDialog;
    private String[] mStrings;
    private String[] pname;
    private int[] pid;
    private String[] pdiscription;
    private String[] pprice;
    private int[] state;
    private   ImageView cart_main,imageView4;
    private JSONArray jsonarray = null; // contacts JSONArray
    private LazyAdapter adapter;
    private String[] val;
    private HashMap<String,String[]> cart_list_value=new HashMap<String,String[]>();
    private CartListDataBase cartdata;
    private ListView list;
    private   static TextView cart_no;

    //TODO  Called when the activity is first created.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list);
        cartdata=MainActivity.cartdata;
        list = (ListView) findViewById(R.id.list);
        cart_main = (ImageView) findViewById(R.id.image_cart_in_list);
        imageView4= (ImageView) findViewById(R.id.imageView4);
        cart_no = (TextView) findViewById(R.id.textview_cart_no);
        cart_no.bringToFront();

        cart_main.setOnClickListener(new View.OnClickListener() {
            //TODO on click of cart button it will navigate to cart list activity
            @Override
            public void onClick(View view) {
                if(Integer.parseInt(cart_no.getText().toString())!=0) {
                    Intent summary_page = new Intent(ProductList.this, CartList.class);
                    startActivity(summary_page);
                }

            }
        });
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(ProductList.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

            }
        });
        //TODO Start ASYNC TAsk
        new GetProducts().execute();

    }

    //TODO Update cart number from adapter
    public static void UpdateCartValue(int count) {
        cart_no.setText("" + count);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //TODO cart number needs to update again and again
        cart_list_value = cartdata.getAllCart();
        cart_no.setText("" + cart_list_value.size());
    }


    public class GetProducts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ProductList.this);
            pDialog.setMessage(getResources().getText(R.string.please_wait));
            pDialog.setCancelable(false);
            pDialog.show();
        }

        boolean isSuccess=true;
        @Override
        protected Void doInBackground(Void... voids) {

            ServiceHandler sh = new ServiceHandler();
            String jsonStr = sh.makeServiceCall(AppConstant.url, ServiceHandler.POST);
            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                if (jsonStr.equals("TimeOut")) {

                    isSuccess=false;

                } else {

                    try {
                        JSONObject jsonObj = new JSONObject(jsonStr);


                        //TODO Getting JSON Array node
                        jsonarray = jsonObj.getJSONArray(AppConstant.PRODUCT_LIST);
                        pname = new String[jsonarray.length()];
                        pdiscription = new String[jsonarray.length()];
                        mStrings = new String[jsonarray.length()];
                        pprice = new String[jsonarray.length()];
                        state = new int[jsonarray.length()];
                        pid = new int[jsonarray.length()];
                        cart_list_value = cartdata.getAllCart();
                        //TODO looping through All Products
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject c = jsonarray.getJSONObject(i);

                            int id = c.getInt(AppConstant.PRODUCT_ID);
                            String name = c.getString(AppConstant.PRODUCT_NAME);
                            String description = c.getString(AppConstant.PRODUCT_DESCRIPTION);
                            String price = c.getString(AppConstant.PRODUCT_PRICE);
                            int status = c.getInt(AppConstant.PRODUCT_STATUS);
                            String imageurl = c.getString(AppConstant.PRODUCT_IMAGE_URL);


                            //TODO USE TO UPDATE CART CHECK AND QUANITY SELECTED BY USER IN DATA BASE
                            for (int j = 0; j < cart_list_value.size(); j++) {
                                val = (String[]) cart_list_value.get("" + j);
                                if (val != null) {
                                    if (Integer.parseInt(val[3]) == (id)) {
                                        cartdata.updateSelected(id, 1);
                                        break;
                                    } else {
                                        cartdata.updateSelected(id, 0);
                                    }
                                } else {
                                    cartdata.updateSelected(id, 0);
                                    cartdata.updateQuanity(id, 1);
                                }
                            }
                            pname[i] = name;
                            pdiscription[i] = description;
                            pprice[i] = price;
                            mStrings[i] = AppConstant.img_url + imageurl;
                            state[i] = status;
                            pid[i] = id;

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }}
                }else{
                    Log.d("ServiceHandler", "Couldn't get any data from the url");

                }
                return null;


        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog.isShowing())
                pDialog.dismiss();
            if(isSuccess){
            //TODO initialise Lazy Adapter
            adapter = new LazyAdapter(ProductList.this, mStrings, pname, pprice, pdiscription, state,pid);
            list.setAdapter(adapter);}else{
                Toast.makeText(ProductList.this, "Connection lost.Please Try Again", Toast.LENGTH_LONG).show();
            }

        }
    }
}