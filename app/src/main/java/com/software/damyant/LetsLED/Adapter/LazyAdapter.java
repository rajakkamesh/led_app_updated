package com.software.damyant.LetsLED.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.Dialog;

import com.software.damyant.LetsLED.Activity.MainActivity;
import com.software.damyant.LetsLED.CartListDataBase;
import com.software.damyant.LetsLED.Activity.ProductList;
import com.software.damyant.LetsLED.R;
import com.software.damyant.LetsLED.lasy_image_loader.ImageLoader;

import java.util.HashMap;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class use for making list view in productList activity
 */
public class LazyAdapter extends BaseAdapter {

    private Context context;
    private String[] data, pname, pprice, pdiscription;
    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;
    public static int[] state, pid;
    private CartListDataBase cartdata;
    private HashMap<String, String[]> cart_list_value = new HashMap<String, String[]>();
    public TextView name, price, dis;

    //TODO Constructor of lazy adaptor
    public LazyAdapter(Context context, String[] d, String[] n, String[] p, String[] dis, int[] state, int[] pid) {
        this.context = context;
        cartdata = MainActivity.cartdata;
        data = d;
        pname = n;
        pprice = p;
        pdiscription = dis;
        this.state = state;
        this.pid = pid;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(context.getApplicationContext());

    }


    public int getCount() {
        return data.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    //TODO called every time and return a view
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.product_list_row_view, null);

        name = (TextView) vi.findViewById(R.id.textview_product_name);
        price = (TextView) vi.findViewById(R.id.textview_product_price);
        dis = (TextView) vi.findViewById(R.id.textview_product_description);
        ImageView image = (ImageView) vi.findViewById(R.id.imageview_product_image);
        final TextView button_addtocart = (TextView) vi.findViewById(R.id.textview_button_addtocart);
        final ImageView imagecheck = (ImageView) vi.findViewById(R.id.imageview_checkcart);
        TextView textview_product_description = (TextView) vi.findViewById(R.id.textview_product_description);
        ImageView imageview_product_image = (ImageView) vi.findViewById(R.id.imageview_product_image);
        cart_list_value = cartdata.getAllCart();
        ProductList.UpdateCartValue(cart_list_value.size());
        //TODO change text and image of button
        Cursor cursor = cartdata.getSelected(pid[position]);
        if (cursor.moveToFirst()) {
            if (Integer.parseInt(cursor.getString(0)) == 1) {
                button_addtocart.setText("Added");
                imagecheck.setVisibility(View.VISIBLE);
                imagecheck.bringToFront();
            } else {

                button_addtocart.setText("Add to cart");
                imagecheck.setVisibility(View.GONE);

            }
        } else {
            button_addtocart.setText("Add to cart");
            imagecheck.setVisibility(View.GONE);
        }
        //TODO Selecting and on selecting of product on click
        button_addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor cursor = cartdata.getSelected(pid[position]);
                if (cursor.moveToFirst()) {
                    //TODO if have product in database already
                    if (Integer.parseInt(cursor.getString(0)) == 0) {
                        cart_list_value = cartdata.getAllCart();
                        ProductList.UpdateCartValue((cart_list_value.size() + 1));
                        button_addtocart.setText("Added");
                        cartdata.updateSelected(pid[position], 1);

                        Cursor cursor1 = cartdata.getQunity(pid[position]);
                        if (cursor1.moveToFirst()) {
                            cartdata.insertData(pid[position], pname[position], Float.parseFloat(pprice[position]), data[position], Integer.parseInt(cursor1.getString(0)), 1);
                        } else {
                            cartdata.insertData(pid[position], pname[position], Float.parseFloat(pprice[position]), data[position], 1, 1);
                        }
                        imagecheck.setVisibility(View.VISIBLE);
                        imagecheck.bringToFront();


                    } else {
                        //TODO product is deselected here
                        cart_list_value = cartdata.getAllCart();
                        ProductList.UpdateCartValue((cart_list_value.size() - 1));
                        button_addtocart.setText("Add to cart");
                        cartdata.updateSelected(pid[position], 0);
                        imagecheck.setVisibility(View.GONE);
                        cartdata.deleteProduct(pid[position]);
                    }

                } else {
                    //TODO if not selected any product previously
                    cart_list_value = cartdata.getAllCart();
                    ProductList.UpdateCartValue((cart_list_value.size() + 1));
                    button_addtocart.setText("Added");
                    cartdata.updateSelected(pid[position], 1);

                    Cursor cursor1 = cartdata.getQunity(pid[position]);
                    if (cursor1.moveToFirst()) {
                        cartdata.insertData(pid[position], pname[position], Float.parseFloat(pprice[position]), data[position], Integer.parseInt(cursor1.getString(0)), 1);
                    } else {
                        cartdata.insertData(pid[position], pname[position], Float.parseFloat(pprice[position]), data[position], 1, 1);
                    }
                    imagecheck.setVisibility(View.VISIBLE);
                    imagecheck.bringToFront();
                }

            }
        });

        //TODO when click on description popup dialog open with description
        textview_product_description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_detail);
                dialog.setTitle("" + pname[position]);
                TextView title = (TextView) dialog.findViewById(R.id.textview_alert_title);
                title.setText("" + pname[position]);
                //TODO set the custom dialog components - text, image and button
                WebView alert_web = (WebView) dialog.findViewById(R.id.textview_alert_detail);
                String youtContentStr = String.valueOf(Html
                        .fromHtml("<![CDATA[<body style=\"text-align:justify;background-color:#00222222;\">"
                                + pdiscription[position] +
                                "</body>]]>"));
                alert_web.setBackgroundColor(Color.TRANSPARENT);
                WebSettings webSettings = alert_web.getSettings();
                webSettings.setDefaultFontSize(12);
                alert_web.loadData(youtContentStr, "text/html", "utf-8");
                dialog.setCanceledOnTouchOutside(false);
                ImageView dialogButton = (ImageView) dialog.findViewById(R.id.button_alert_ok);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }


        });

        //TODO when click on Image pop up open with full size image
        imageview_product_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_detail);
                dialog.setTitle("" + pname[position]);
                TextView title = (TextView) dialog.findViewById(R.id.textview_alert_title);
                title.setText("" + pname[position]);
                // set the custom dialog components - text, image and button
                WebView alert_web = (WebView) dialog.findViewById(R.id.textview_alert_detail);
                alert_web.setBackgroundColor(Color.TRANSPARENT);
                WebSettings webSettings = alert_web.getSettings();
                webSettings.setDefaultFontSize(12);
                alert_web.loadUrl(data[position]);
                dialog.setCanceledOnTouchOutside(false);
                ImageView dialogButton = (ImageView) dialog.findViewById(R.id.button_alert_ok);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }


        });
        name.setText(pname[position]);
        dis.setText(pdiscription[position]);
        price.setText(pprice[position]);
        imageLoader.DisplayImage(data[position], image);
        return vi;
    }

}