package com.software.damyant.LetsLED.Camera;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO class used to auto focus camera
 */
public class AutofocusCrosshair extends View {

    private Point mLocationPoint;

    public AutofocusCrosshair(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void setDrawable(int resid) {
        this.setBackgroundResource(resid);
    }



    public void clear() {
        setBackgroundDrawable(null);
    }

}