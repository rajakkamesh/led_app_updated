package com.software.damyant.LetsLED.Activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.software.damyant.LetsLED.R;

/**
 * TODO Created by Ty@gI on 06-05-2015.
 * TODO  Class use to show user detail form
 */

public class UserDetail extends Activity {
    private ImageView imageView4;
    private Button button_userdetail;
    private String body;
    private LinearLayout ll_home;
    private boolean isNevigate=false;
    private EditText first_name,last_name,address,mob_num,email,comments;

    //TODO  Called when the activity is first created.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_detail);
        imageView4=(ImageView)findViewById(R.id.imageView4);
        button_userdetail=(Button)findViewById(R.id.button_userdetail);
        first_name=(EditText)findViewById(R.id.edit_first_name);
        last_name=(EditText)findViewById(R.id.edit_last_name);
        address=(EditText)findViewById(R.id.edit_address);
        mob_num=(EditText)findViewById(R.id.edit_mob);
        email=(EditText)findViewById(R.id.edit_email);
        ll_home=(LinearLayout)findViewById(R.id.ll_home);
        comments=(EditText)findViewById(R.id.edit_comment);
        //TODO called when click on home button
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TODO activity navigate to Product list if internet present
                Intent i = new Intent(UserDetail.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

            }
        });

        //TODO called when click on home button
        ll_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO activity navigate to home screen
                Intent i = new Intent(UserDetail.this, Room_Chooser.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

        button_userdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                body = "Name : " + first_name.getText().toString() + " " + last_name.getText().toString() + "\nAddress : " + address.getText().toString() + "\nMobile number : " + mob_num.getText().toString() + "\nEmail Id : " + email.getText().toString() + "\nComment : " + comments.getText().toString();
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches() && android.util.Patterns.PHONE.matcher(mob_num.getText().toString()).matches()) {
                    sendEmail(body);
                    isNevigate=true;

                } else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
                    Toast.makeText(UserDetail.this, "Invalid Email Address",
                            Toast.LENGTH_SHORT).show();

                }else if(!android.util.Patterns.PHONE.matcher(mob_num.getText().toString()).matches()){
                    Toast.makeText(UserDetail.this, "Invalid phone number",
                            Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

    //TODO called when activity resume
    @Override
    protected void onResume() {
        super.onResume();
        if(isNevigate) {
            Intent i = new Intent(UserDetail.this, Room_Chooser.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }
    private void sendEmail(String body){

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:" + "letsled@moserbaer.in"));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Customer Enquiry");
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send email using..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(UserDetail.this, "No email clients installed.", Toast.LENGTH_SHORT).show();
        }

    }

}
